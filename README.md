# OpenML dataset: telco-customer-churn

https://www.openml.org/d/45568

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
"Predict behavior to retain customers. You can analyze all relevant customer data and develop focused customer retention programs." [IBM Sample Data Sets]

Content
Each row represents a customer, each column contains customer's attributes described on the column Metadata.

The data set includes information about:

Customers who left within the last month - the column is called Churn
Services that each customer has signed up for - phone, multiple lines, internet, online security, online backup, device protection, tech support, and streaming TV and movies
Customer account information - how long they've been a customer, contract, payment method, paperless billing, monthly charges, and total charges
Demographic info about customers - gender, age range, and if they have partners and dependents
Inspiration
To explore this type of models and learn more about the subject.

Taken from Kaggle: https://www.kaggle.com/blastchar/telco-customer-churn/download

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45568) of an [OpenML dataset](https://www.openml.org/d/45568). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45568/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45568/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45568/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

